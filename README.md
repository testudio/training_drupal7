# Drupal 7 training

## Setup: Quick start

1. Get a machine with Docker and Docker compose.
We recommend using [Digital Ocean](https://cloud.digitalocean.com/).

2. Pull repository

```
mkdir -p /app
git clone https://gitlab.com/testudio/training_drupal7.git /app/drupal7
cd /app/drupal7
docker-compose up -d --build
```

Test running containers

```
 docker-compose ps
```

3. `Optional`. Create startup script

```
crontab -e
@reboot cd /app/drupal7 && /usr/local/bin/docker-compose up -d
```

4. Install Drupal

```
docker exec -it web composer install # Get composer dependencies
docker exec -it web composer site:install-docker # Install drupal
docker exec -it web composer drush-uli # Login into Drupal
# OR docker exec -it web ./vendor/bin/drush -l default user:login --uri=http://178.128.213.12
```

## Links

* For Drupal 8 training head to this [repo](https://gitlab.com/testudio/drupal-gtd).
* [Tomato Elephant Studio](https://tomato-elephant-studio.com)
* [Drupal Global Training Day](https://groups.drupal.org/node/512931)
* [Drupal](https://www.drupal.org/)
